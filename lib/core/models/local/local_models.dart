import 'package:hive/hive.dart';

part 'local_models.g.dart';

@HiveType(typeId: 1)
class AccountData {
  @HiveField(0)
  String? id;
  @HiveField(1)
  String? firstName;
  @HiveField(2)
  String? lastName;
  @HiveField(3)
  String? phoneNumber;
  @HiveField(4)
  String? email;
  @HiveField(5)
  String? role;

  AccountData({
    this.id,
    this.firstName,
    this.lastName,
    this.phoneNumber,
    this.email,
    this.role,
  });
  AccountData.fromJson(Map<String, dynamic> json) {
    id = json["id"]?.toString();
    firstName = json["firstName"]?.toString();
    lastName = json["lastName"]?.toString();
    phoneNumber = json["phoneNumber"]?.toString();
    email = json["email"]?.toString();
    role = json["role"]?.toString();
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["id"] = id;
    data["firstName"] = firstName;
    data["lastName"] = lastName;
    data["phoneNumber"] = phoneNumber;
    data["email"] = email;
    data["role"] = role;
    return data;
  }
}
