import 'package:artisto/core/enums/view_state.dart';
import 'package:artisto/core/models/create_account_response.dart';
import 'package:artisto/core/services/api.dart';

import '../../locator.dart';
import 'base_model.dart';

class LoginModel extends BaseModel {
  Api _api = locator<Api>();

  Future<CreateAccountResponse> loginUser(Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.loginUser(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<CreateAccountResponse> registerUser(Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.registerUser(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
