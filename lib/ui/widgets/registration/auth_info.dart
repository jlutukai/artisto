import 'package:artisto/ui/pages/login_page.dart';
import 'package:artisto/utils/constants.dart';
import 'package:flutter/material.dart';

class AuthInfo extends StatefulWidget {
  final void Function(int index) onDataChange;
  AuthInfo(this.onDataChange);

  @override
  _AuthInfoState createState() => _AuthInfoState();
}

class _AuthInfoState extends State<AuthInfo> {
  TextEditingController _password = TextEditingController();
  TextEditingController _confirmPassword = TextEditingController();
  String pass = "";
  String confirmPass = "";
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: fromHex(background),
      body: Column(
        children: [
          SizedBox(
            height: 35,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Set Password',
                style: TextStyle(
                    color: fromHex(blue),
                    fontSize: 26,
                    fontWeight: FontWeight.w100),
              ),
            ],
          ),
          SizedBox(
            height: 35,
          ),
          Expanded(
              child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Form(
              key: _formKey,
              child: Container(
                height: MediaQuery.of(context).size.height * 0.61,
                child: Column(
                  children: [
                    Expanded(
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 25),
                            child: PhysicalModel(
                              elevation: 5,
                              color: Colors.white,
                              borderRadius: BorderRadius.all(
                                Radius.circular(10),
                              ),
                              child: TextFormField(
                                controller: _password,
                                keyboardType: TextInputType.visiblePassword,
                                obscureText: true,
                                style: TextStyle(color: Colors.black),
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return "Please Enter your password";
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.white,
                                  isDense: true,
                                  hintText: "*******",
                                  labelText: "Password",
                                  labelStyle: TextStyle(color: fromHex(grey)),
                                  hintStyle: TextStyle(
                                    color: Colors.blueGrey[400],
                                  ),
                                  border: getBorder(),
                                  enabledBorder: getBorder(),
                                  focusedBorder: getEnabledBorder(),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 25),
                            child: PhysicalModel(
                              elevation: 5,
                              color: Colors.white,
                              borderRadius: BorderRadius.all(
                                Radius.circular(10),
                              ),
                              child: TextFormField(
                                controller: _password,
                                keyboardType: TextInputType.visiblePassword,
                                obscureText: true,
                                style: TextStyle(color: Colors.black),
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return "Please Re-Enter your password";
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.white,
                                  isDense: true,
                                  hintText: "*******",
                                  labelText: "Confirm Password",
                                  labelStyle: TextStyle(color: fromHex(grey)),
                                  hintStyle: TextStyle(
                                    color: Colors.blueGrey[400],
                                  ),
                                  border: getBorder(),
                                  enabledBorder: getBorder(),
                                  focusedBorder: getEnabledBorder(),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 25,
                          ),
                        ],
                      ),
                    ),
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 25),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    child: Row(
                                      children: [
                                        RaisedButton(
                                          elevation: 5.0,
                                          onPressed: () async {
                                            widget.onDataChange(0);
                                            // if (_formKey.currentState.validate()) {
                                            //   // Map<String, String> data = {
                                            //   //   "personal_identification_number":
                                            //   //   "${_userName.text}",
                                            //   //   "password": "${_password.text}"
                                            //   // };
                                            //   // try {
                                            //   //   var r = await model.loginUser(data);
                                            //   //   showToast(r.message);
                                            //   //   // setCurrentUser(r.user);
                                            //   //   Navigator.pushNamedAndRemoveUntil(
                                            //   //       context,
                                            //   //       HomePage.tag,
                                            //   //           (Route<dynamic> route) => false);
                                            //   // } catch (e) {
                                            //   //   try {
                                            //   //     Map<String, dynamic> error =
                                            //   //     jsonDecode(e.toString());
                                            //   //     showToast(
                                            //   //         error['errors'][0]['message']);
                                            //   //   } catch (a) {}
                                            //   // }
                                            // }
                                          },
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0)),
                                          color: Colors.white,
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 10.0),
                                            child: Center(
                                              child: Row(
                                                children: [
                                                  Icon(
                                                    Icons
                                                        .arrow_back_ios_rounded,
                                                    size: 16,
                                                    color: fromHex(blue),
                                                  ),
                                                  SizedBox(
                                                    width: 10,
                                                  ),
                                                  Text(
                                                    'Back',
                                                    style: TextStyle(
                                                        color: fromHex(blue),
                                                        fontSize: 18),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  // model.state == ViewState.Idle
                                  //     ?
                                  Expanded(
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        RaisedButton(
                                          elevation: 5.0,
                                          onPressed: () async {
                                            // widget.onDataChange(1);
                                            // if (_formKey.currentState.validate()) {
                                            //   // Map<String, String> data = {
                                            //   //   "personal_identification_number":
                                            //   //   "${_userName.text}",
                                            //   //   "password": "${_password.text}"
                                            //   // };
                                            //   // try {
                                            //   //   var r = await model.loginUser(data);
                                            //   //   showToast(r.message);
                                            //   //   // setCurrentUser(r.user);
                                            Navigator.pushNamedAndRemoveUntil(
                                                context,
                                                LoginPage.tag,
                                                (Route<dynamic> route) =>
                                                    false);
                                            //   // } catch (e) {
                                            //   //   try {
                                            //   //     Map<String, dynamic> error =
                                            //   //     jsonDecode(e.toString());
                                            //   //     showToast(
                                            //   //         error['errors'][0]['message']);
                                            //   //   } catch (a) {}
                                            //   // }
                                            // }
                                          },
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0)),
                                          color: fromHex(dark_purple),
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 10.0),
                                            child: Center(
                                              child: Row(
                                                children: [
                                                  Text(
                                                    'Finish',
                                                    style: TextStyle(
                                                        color: fromHex(blue),
                                                        fontSize: 18),
                                                  ),
                                                  SizedBox(
                                                    width: 10,
                                                  ),
                                                  Icon(
                                                    Icons.done,
                                                    color: fromHex(blue),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                  // : loader(),
                                ],
                              ),
                              SizedBox(
                                height: 35,
                              ),
                            ],
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          )),
        ],
      ),
    );
  }
}
