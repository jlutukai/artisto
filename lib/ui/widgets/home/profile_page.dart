import 'package:artisto/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: fromHex(background),
      body: Column(
        children: [
          Stack(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.25,
                decoration: BoxDecoration(
                  color: fromHex(maroon),
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(55),
                  ),
                ),
              ),
              Positioned(
                right: -MediaQuery.of(context).size.width * 0.10,
                bottom: 0,
                child: SvgPicture.asset(
                  "assets/logo.svg",
                  color: fromHex(white).withOpacity(0.15),
                  width: MediaQuery.of(context).size.height * 0.20,
                  height: MediaQuery.of(context).size.height * 0.20,
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.25,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 30, bottom: 30),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Row(
                          children: [
                            Text(
                              "Manage your account",
                              style: TextStyle(
                                  color: fromHex(white),
                                  fontWeight: FontWeight.w900,
                                  fontSize: 24),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                              "${getCurrentUser().firstName}'s Profile info...",
                              style: TextStyle(
                                  color: fromHex(white),
                                  fontWeight: FontWeight.w100,
                                  fontSize: 12),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
          Expanded(
            child: Container(),
          ),
        ],
      ),
    );
  }
}
