import 'dart:convert';

import 'package:artisto/core/enums/view_state.dart';
import 'package:artisto/core/viewmodels/request_password_reset_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../utils/constants.dart';
import 'base_view.dart';
import 'otp_page.dart';

class ResetPassword extends StatefulWidget {
  static const tag = 'reset_password';
  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  TextEditingController _email = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return BaseView<RequestPasswordResetModel>(
      builder: (context, model, child) => Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.white,
        body: Column(
          children: [
            Stack(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height * 0.25,
                  decoration: BoxDecoration(color: fromHex(maroon)),
                ),
                Positioned(
                  right: -200,
                  top: -50,
                  child: Container(
                    width: MediaQuery.of(context).size.height * 0.50,
                    height: MediaQuery.of(context).size.height * 0.50,
                    decoration: new BoxDecoration(
                      color: fromHex(light_maroon),
                      shape: BoxShape.circle,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.25,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20, bottom: 20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Row(
                            children: [
                              Text(
                                "Reset Password",
                                style: TextStyle(
                                    color: fromHex(blue),
                                    fontWeight: FontWeight.w900,
                                    fontSize: 24),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                "Enter account email to request...",
                                style: TextStyle(
                                    color: fromHex(blue),
                                    fontWeight: FontWeight.w100,
                                    fontSize: 12),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Expanded(
              child: _loginDetails(context, model),
            ),
          ],
        ),
      ),
    );
  }

  _loginDetails(BuildContext context, RequestPasswordResetModel model) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Form(
        key: _formKey,
        child: Container(
          height: MediaQuery.of(context).size.height * 0.75,
          color: Colors.white,
          child: Column(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(
                      bottom: 40, left: 35, right: 35, top: 40),
                  child: Column(
                    children: [
                      ClipOval(
                        child: SvgPicture.asset(
                          "assets/logo.svg",
                          height: 100,
                          width: 100,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        'Revision Plus',
                        style: TextStyle(
                            color: fromHex(blue), fontWeight: FontWeight.w800),
                      ),
                      Text('Revision Made Easy',
                          style: GoogleFonts.pacifico(
                            textStyle: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w100,
                                color: fromHex(grey)),
                          ))
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 25),
                      child: PhysicalModel(
                        elevation: 5,
                        color: Colors.white,
                        borderRadius: BorderRadius.all(
                          Radius.circular(10),
                        ),
                        child: TextFormField(
                          controller: _email,
                          keyboardType: TextInputType.emailAddress,
                          style: TextStyle(color: Colors.black),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "Please Enter email address";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            isDense: true,
                            hintText: "johndoe@email.com",
                            labelText: "Email Address",
                            labelStyle: TextStyle(color: fromHex(grey)),
                            hintStyle: TextStyle(
                              color: Colors.blueGrey[400],
                            ),
                            border: getBorder(),
                            enabledBorder: getBorder(),
                            focusedBorder: getEnabledBorder(),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  model.state == ViewState.Idle
                      ? RaisedButton(
                          elevation: 5.0,
                          onPressed: () async {
                            bool hasConnection = await checkConnection();
                            if (!hasConnection) {
                              showToast("Check Internet Connection");
                              return;
                            }
                            if (_formKey.currentState!.validate()) {
                              Map<String, String> data = {
                                "personal_identification_number":
                                    "${_email.text}",
                              };
                              try {
                                // var r = await model.requestPasswordReset(data);
                                // showToast(r?.message ?? '');
                                Navigator.of(context).pushAndRemoveUntil(
                                    MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          OTPPage("reset", _email.text),
                                    ),
                                    (Route<dynamic> route) => false);
                              } catch (e) {
                                try {
                                  Map<String, dynamic> error =
                                      jsonDecode(e.toString());
                                  showToast(error['errors'][0]['message']);
                                } catch (a) {}
                              }
                            }
                          },
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                          color: fromHex(dark_purple),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10.0),
                            child: Center(
                              child: Row(
                                children: [
                                  Text(
                                    'Request',
                                    style: TextStyle(
                                        color: fromHex(blue), fontSize: 18),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        )
                      : loader(),
                  SizedBox(
                    width: 25,
                  )
                ],
              ),
              SizedBox(
                height: 40,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
