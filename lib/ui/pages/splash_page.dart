import 'dart:async';

import 'package:artisto/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'home_page.dart';
import 'login_page.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Timer(Duration(seconds: 5), () {
      if (getToken().isNotEmpty) {
        Navigator.pushNamedAndRemoveUntil(
            context, HomePage.tag, (Route<dynamic> route) => false);
      } else {
        Navigator.pushNamedAndRemoveUntil(
            context, LoginPage.tag, (Route<dynamic> route) => false);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: fromHex(light_maroon),
      body: Column(
        children: [
          Expanded(
              child: Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  // Text(
                  //   'Welcome',
                  //   style: TextStyle(fontSize: 24, fontWeight: FontWeight.w800),
                  // ),
                  SizedBox(
                    height: 15,
                  ),
                  SvgPicture.asset(
                    "assets/logo.svg",
                    color: fromHex(white),
                    height: 100,
                    width: 100,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    'Artisto',
                    style: TextStyle(
                        color: fromHex(white),
                        fontWeight: FontWeight.w100,
                        fontSize: 26),
                  )
                ],
              ),
            ),
          ))
        ],
      ),
    );
  }
}
