import 'dart:convert';

import 'package:artisto/core/enums/view_state.dart';
import 'package:artisto/core/viewmodels/otp_model.dart';
import 'package:artisto/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:sms_otp_auto_verify/sms_otp_auto_verify.dart';

import 'base_view.dart';
import 'change_password_page.dart';
import 'login_page.dart';

class OTPPage extends StatefulWidget {
  final String s;
  final String email;

  OTPPage(this.s, this.email);
  @override
  _OTPPageState createState() => _OTPPageState();
}

class _OTPPageState extends State<OTPPage> {
  int _otpCodeLength = 6;
  bool _isLoadingButton = false;
  bool _enableButton = false;
  String _otpCode = "";
  String email = "";
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    email = widget.email;
    _getSignatureCode();
    super.initState();
  }

  /// get signature code
  _getSignatureCode() async {
    String signature = await SmsRetrieved.getAppSignature();
    print("signature $signature");
  }

  _onSubmitOtp(OTPModel model) async {
    bool hasConnection = await checkConnection();
    if (!hasConnection) {
      showToast("Check Internet Connection");
      return;
    }
    setState(() {
      _isLoadingButton = !_isLoadingButton;
      _verifyOtpCode(model);
    });
  }

  _onOtpCallBack(String otpCode, bool isAutoFill, OTPModel model) async {
    bool hasConnection = await checkConnection();
    if (!hasConnection) {
      showToast("Check Internet Connection");
      return;
    }
    setState(() {
      this._otpCode = otpCode;
      if (otpCode.length == _otpCodeLength && isAutoFill) {
        _enableButton = false;
        _isLoadingButton = true;
        _verifyOtpCode(model);
      } else if (otpCode.length == _otpCodeLength && !isAutoFill) {
        _enableButton = true;
        _isLoadingButton = false;
      } else {
        _enableButton = false;
      }
    });
  }

  _verifyOtpCode(OTPModel model) async {
    FocusScope.of(context).requestFocus(new FocusNode());
    if (widget.s == "login") {
      Map<String, String> data = {"code": "$_otpCode"};
      try {
        // var r = await model.verifyOTP(data);
        // showToast(r?.message ?? r?.status ?? '');
        // CreateAccountBody c = getAddUserData();
        // c.hasActivatedAccount = true;
        // setAddUserData(c);
        setState(() {
          _isLoadingButton = false;
          _enableButton = false;
        });
        deleteAddUserData();
        Navigator.pushNamedAndRemoveUntil(
            context, LoginPage.tag, (Route<dynamic> route) => false);
      } catch (e) {
        setState(() {
          _isLoadingButton = false;
          _enableButton = false;
        });
        try {
          Map<String, dynamic> error = jsonDecode(e.toString());
          showToast(error['errors'][0]['message']);
        } catch (a) {}
      }
    } else {
      setState(() {
        _isLoadingButton = !_isLoadingButton;
      });
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => ChangePasswordPage(_otpCode)),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<OTPModel>(
      onModelReady: (model) async {
        bool hasConnection = await checkConnection();
        if (!hasConnection) {
          showToast("Check Internet Connection");
          return;
        }
      },
      builder: (context, model, child) => Scaffold(
        backgroundColor: fromHex(background),
        resizeToAvoidBottomInset: true,
        body: Container(
          child: Column(
            children: [
              Stack(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height * 0.25,
                    decoration: BoxDecoration(color: fromHex(maroon)),
                  ),
                  Positioned(
                    right: -200,
                    top: -50,
                    child: Container(
                      width: MediaQuery.of(context).size.height * 0.50,
                      height: MediaQuery.of(context).size.height * 0.50,
                      decoration: new BoxDecoration(
                        color: fromHex(light_maroon),
                        shape: BoxShape.circle,
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      height: MediaQuery.of(context).size.height * 0.25,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20, bottom: 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Row(
                              children: [
                                Text(
                                  "Account Verification",
                                  style: TextStyle(
                                      color: fromHex(blue),
                                      fontWeight: FontWeight.w900,
                                      fontSize: 24),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  "Check code sent to your email...",
                                  style: TextStyle(
                                      color: fromHex(blue),
                                      fontWeight: FontWeight.w100,
                                      fontSize: 12),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Expanded(
                  child: model.state == ViewState.Idle
                      ? SingleChildScrollView(
                          physics: BouncingScrollPhysics(),
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.75,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 35, right: 35, top: 40),
                                  child: Column(
                                    children: [
                                      ClipOval(
                                        child: SvgPicture.asset(
                                          "assets/logo.svg",
                                          height: 100,
                                          width: 100,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        'Revision Plus',
                                        style: TextStyle(
                                            color: fromHex(blue),
                                            fontWeight: FontWeight.w800),
                                      ),
                                      Text('Revision Made Easy',
                                          style: GoogleFonts.pacifico(
                                            textStyle: TextStyle(
                                                fontSize: 10,
                                                fontWeight: FontWeight.w100,
                                                color: fromHex(grey)),
                                          ))
                                    ],
                                  ),
                                ),
                                TextFieldPin(
                                    filled: true,
                                    filledColor: fromHex(grey).withOpacity(0.2),
                                    codeLength: _otpCodeLength,
                                    boxSize: 46,
                                    filledAfterTextChange: false,
                                    textStyle: TextStyle(fontSize: 16),
                                    borderStyle: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: fromHex(dark_purple),
                                            width: 2),
                                        borderRadius:
                                            BorderRadius.circular(34)),
                                    onOtpCallback: (code, isAutoFill) async {
                                      await _onOtpCallBack(
                                          code, isAutoFill, model);
                                    }),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    InkWell(
                                      onTap: () async {
                                        bool hasConnection =
                                            await checkConnection();
                                        if (!hasConnection) {
                                          showToast(
                                              "Check Internet Connection");
                                          return;
                                        }
                                        Map<String, String> data = {
                                          "username": "$email"
                                        };
                                        try {
                                          // var r = await model.resendOTP(data);
                                          // showToast(r.message);
                                        } catch (e) {
                                          try {
                                            Map<String, dynamic> error =
                                                jsonDecode(e.toString());
                                            showToast(
                                                error['errors'][0]['message']);
                                          } catch (a) {}
                                        }
                                      },
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            "Didn't Receive Code? ",
                                            style: TextStyle(
                                              color: Colors.black,
                                            ),
                                          ),
                                          Text(
                                            "Resend",
                                            style: TextStyle(
                                                color: fromHex(dark_purple),
                                                fontWeight: FontWeight.w800),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 20, right: 20),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          _isLoadingButton
                                              ? loader()
                                              : RaisedButton(
                                                  elevation: 5.0,
                                                  onPressed: _enableButton
                                                      ? () async {
                                                          await _onSubmitOtp(
                                                              model);
                                                        }
                                                      : null,
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10.0)),
                                                  color: fromHex(dark_purple),
                                                  child: Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        vertical: 10.0),
                                                    child: Center(
                                                      child: Row(
                                                        children: [
                                                          Text(
                                                            'Verify',
                                                            style: TextStyle(
                                                                color: fromHex(
                                                                    blue),
                                                                fontSize: 18),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                        ],
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                      : loader()),
              SizedBox(
                height: 30,
              )
            ],
          ),
        ),
      ),
    );
  }
}
//702660953
