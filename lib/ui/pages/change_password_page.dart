import 'dart:convert';

import 'package:artisto/core/enums/view_state.dart';
import 'package:artisto/core/viewmodels/request_password_reset_model.dart';
import 'package:artisto/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import 'base_view.dart';
import 'login_page.dart';

class ChangePasswordPage extends StatefulWidget {
  final String code;
  ChangePasswordPage(this.code);
  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  TextEditingController _pass = TextEditingController();
  TextEditingController _confirmPass = TextEditingController();
  String pass = "";
  String confirmPass = "";

  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return BaseView<RequestPasswordResetModel>(
      builder: (context, model, child) => Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.white,
        body: Column(
          children: [
            Stack(
              children: [
                Stack(
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.height * 0.25,
                      decoration: BoxDecoration(color: fromHex(maroon)),
                    ),
                    Positioned(
                      right: -200,
                      top: -50,
                      child: Container(
                        width: MediaQuery.of(context).size.height * 0.50,
                        height: MediaQuery.of(context).size.height * 0.50,
                        decoration: new BoxDecoration(
                          color: fromHex(light_maroon),
                          shape: BoxShape.circle,
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        height: MediaQuery.of(context).size.height * 0.25,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 20, bottom: 20),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Row(
                                children: [
                                  Text(
                                    "Reset Password",
                                    style: TextStyle(
                                        color: fromHex(blue),
                                        fontWeight: FontWeight.w900,
                                        fontSize: 24),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  Text(
                                    "Create a new password...",
                                    style: TextStyle(
                                        color: fromHex(blue),
                                        fontWeight: FontWeight.w100,
                                        fontSize: 12),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                model.state == ViewState.Idle
                    ? Positioned(
                        left: 20,
                        top: MediaQuery.of(context).size.height * 0.1 - 35,
                        child: FloatingActionButton(
                          foregroundColor: Colors.grey,
                          backgroundColor: fromHex(dark_purple),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          mini: true,
                          tooltip: "go back",
                          child: Icon(
                            Icons.keyboard_backspace_sharp,
                            color: fromHex(blue),
                          ),
                        ),
                      )
                    : Container(),
              ],
            ),
            Expanded(
                child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: _loginDetails(context, model),
            )),
          ],
        ),
      ),
    );
  }

  _loginDetails(BuildContext context, RequestPasswordResetModel model) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.75,
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.only(bottom: 60, left: 25, right: 25),
                    child: Column(
                      children: [
                        ClipOval(
                          child: SvgPicture.asset(
                            "assets/logo.svg",
                            height: 100,
                            width: 100,
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          'Revision Plus',
                          style: TextStyle(
                              color: fromHex(blue),
                              fontWeight: FontWeight.w800),
                        ),
                        Text('Revision Made Easy',
                            style: GoogleFonts.pacifico(
                              textStyle: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w100,
                                  color: fromHex(grey)),
                            ))
                      ],
                    ),
                  ),
                  Column(
                    children: [
                      Container(
                        margin:
                            EdgeInsets.symmetric(horizontal: 25, vertical: 5),
                        child: PhysicalModel(
                          elevation: 5,
                          color: Colors.white,
                          borderRadius: BorderRadius.all(
                            Radius.circular(10),
                          ),
                          child: TextFormField(
                            controller: _pass,
                            keyboardType: TextInputType.visiblePassword,
                            obscureText: true,
                            style: TextStyle(color: Colors.black),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Please Enter your pass word";
                              }
                              if (confirmPass != value) {
                                return "Passwords do not match";
                              }
                              return null;
                            },
                            onChanged: (value) {
                              setState(() {
                                pass = value;
                              });
                            },
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white,
                              isDense: true,
                              hintText: "************",
                              labelText: "New Password",
                              labelStyle: TextStyle(color: fromHex(grey)),
                              hintStyle: TextStyle(
                                color: Colors.blueGrey[400],
                              ),
                              border: getBorder(),
                              enabledBorder: getBorder(),
                              focusedBorder: getEnabledBorder(),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        margin:
                            EdgeInsets.symmetric(horizontal: 25, vertical: 5),
                        child: PhysicalModel(
                          elevation: 5,
                          color: Colors.white,
                          borderRadius: BorderRadius.all(
                            Radius.circular(10),
                          ),
                          child: TextFormField(
                            controller: _confirmPass,
                            keyboardType: TextInputType.visiblePassword,
                            obscureText: true,
                            style: TextStyle(color: Colors.black),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Please Re-Enter your password";
                              }
                              if (pass != value) {
                                return "Passwords do not match";
                              }
                              return null;
                            },
                            onChanged: (value) {
                              setState(() {
                                confirmPass = value;
                              });
                            },
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white,
                              isDense: true,
                              hintText: "*********",
                              labelText: "Confirm New Password",
                              labelStyle: TextStyle(color: fromHex(grey)),
                              hintStyle: TextStyle(
                                color: Colors.blueGrey[400],
                              ),
                              border: getBorder(),
                              enabledBorder: getBorder(),
                              focusedBorder: getEnabledBorder(),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                model.state == ViewState.Idle
                    ? RaisedButton(
                        elevation: 5.0,
                        onPressed: () async {
                          bool hasConnection = await checkConnection();
                          if (!hasConnection) {
                            showToast("Check Internet Connection");
                            return;
                          }
                          if (_formKey.currentState!.validate()) {
                            Map<String, String> data = {
                              "code": "${widget.code}",
                              "password": "${_pass.text}",
                              "password_confirm": "${_confirmPass.text}"
                            };
                            try {
                              // var r = await model.resetPassword(data);
                              // showToast(r?.message ?? '');
                              Navigator.pushNamedAndRemoveUntil(
                                  context,
                                  LoginPage.tag,
                                  (Route<dynamic> route) => false);
                            } catch (e) {
                              try {
                                Map<String, dynamic> error =
                                    jsonDecode(e.toString());
                                showToast(error['errors'][0]['message']);
                              } catch (a) {}
                            }
                          }
                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0)),
                        color: fromHex(dark_purple),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10.0),
                          child: Center(
                            child: Row(
                              children: [
                                Text(
                                  'Reset Password',
                                  style: TextStyle(
                                      color: fromHex(blue), fontSize: 18),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Icon(
                                  Icons.arrow_forward_ios,
                                  size: 16,
                                  color: fromHex(blue),
                                ),
                              ],
                            ),
                          ),
                        ),
                      )
                    : loader(),
                SizedBox(
                  width: 25,
                )
              ],
            ),
            SizedBox(
              height: 40,
            ),
          ],
        ),
      ),
    );
  }
}
