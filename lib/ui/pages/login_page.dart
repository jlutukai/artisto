import 'dart:convert';

import 'package:artisto/core/enums/view_state.dart';
import 'package:artisto/core/viewmodels/login_model.dart';
import 'package:artisto/ui/pages/registration_page.dart';
import 'package:artisto/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'base_view.dart';
import 'home_page.dart';

class LoginPage extends StatefulWidget {
  static const tag = 'login';
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _isHidden = true;
  String pass = "";
  TextEditingController _userName = TextEditingController();
  TextEditingController _password = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<LoginModel>(
      builder: (context, model, child) => Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: fromHex(background),
        body: Column(
          children: [
            Stack(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height * 0.25,
                  decoration: BoxDecoration(
                    color: fromHex(maroon),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(55),
                    ),
                  ),
                ),
                Positioned(
                  right: -MediaQuery.of(context).size.width * 0.10,
                  bottom: 0,
                  child: SvgPicture.asset(
                    "assets/logo.svg",
                    color: fromHex(white).withOpacity(0.15),
                    width: MediaQuery.of(context).size.height * 0.20,
                    height: MediaQuery.of(context).size.height * 0.20,
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.25,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 30, bottom: 30),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Row(
                            children: [
                              Text(
                                "Welcome Back",
                                style: TextStyle(
                                    color: fromHex(white),
                                    fontWeight: FontWeight.w900,
                                    fontSize: 24),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                "Login to continue...",
                                style: TextStyle(
                                    color: fromHex(white),
                                    fontWeight: FontWeight.w100,
                                    fontSize: 12),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Expanded(
              child: Container(
                child: _loginDetails(context, model),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _toggleVisibility() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }

  _loginDetails(BuildContext context, LoginModel model) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Form(
        key: _formKey,
        child: Container(
          height: MediaQuery.of(context).size.height * 0.75,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Center(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25),
                  child: Column(
                    children: [
                      SvgPicture.asset(
                        "assets/logo.svg",
                        color: fromHex(maroon),
                        height: 100,
                        width: 100,
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        'Artisto',
                        style: TextStyle(
                            color: fromHex(maroon),
                            fontSize: 18,
                            fontWeight: FontWeight.w100),
                      ),
                      // Text('Revision Made Easy',
                      //     style: GoogleFonts.pacifico(
                      //       textStyle: TextStyle(
                      //           fontSize: 10,
                      //           fontWeight: FontWeight.w100,
                      //           color: fromHex(grey)),
                      //     ))
                    ],
                  ),
                ),
              ),
              Column(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    decoration: BoxDecoration(
                      color: fromHex(light_maroon).withOpacity(0.05),
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(15),
                      ),
                    ),
                    child: TextFormField(
                      controller: _userName,
                      keyboardType: TextInputType.text,
                      textCapitalization: TextCapitalization.words,
                      style: TextStyle(color: Colors.black),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "Please Enter Email";
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white10,
                        isDense: true,
                        hintText: "e.g. johndoe@example.com",
                        labelText: "Email",
                        labelStyle: TextStyle(color: fromHex(black)),
                        hintStyle: TextStyle(
                          color: fromHex(grey),
                        ),
                        border: getBorder(),
                        focusedBorder: getEnabledBorder(),
                        enabledBorder: getBorder(),
                        prefixIcon: Icon(
                          Icons.mail_outline,
                          color: fromHex(maroon),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    decoration: BoxDecoration(
                      color: fromHex(light_maroon).withOpacity(0.05),
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(15),
                      ),
                    ),
                    child: TextFormField(
                      controller: _password,
                      keyboardType: TextInputType.visiblePassword,
                      obscureText: _isHidden,
                      style: TextStyle(color: Colors.black),
                      onChanged: (val) {
                        pass = val;
                        setState(() {});
                      },
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "Please your password";
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white10,
                        isDense: true,
                        labelText: "Password",
                        labelStyle: TextStyle(color: fromHex(black)),
                        hintText: "Your password",
                        hintStyle: TextStyle(
                          color: fromHex(grey),
                        ),
                        border: getBorder(),
                        enabledBorder: getBorder(),
                        focusedBorder: getEnabledBorder(),
                        prefixIcon: Icon(
                          Icons.lock_open_outlined,
                          color: fromHex(maroon),
                        ),
                        suffixIcon: pass.isNotEmpty
                            ? IconButton(
                                onPressed: _toggleVisibility,
                                icon: _isHidden
                                    ? Icon(
                                        Icons.visibility_off,
                                        color: Colors.grey[400],
                                      )
                                    : Icon(
                                        Icons.visibility,
                                        color: fromHex(dark_purple),
                                      ),
                              )
                            : null,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 30, top: 25),
                    child: Row(
                      children: [
                        Text('Forgot Password ? '),
                        GestureDetector(
                          onTap: () {
                            // if (model.state == ViewState.Idle) {
                            //   Navigator.pushNamedAndRemoveUntil(
                            //       context,
                            //       ResetPassword.tag,
                            //       (Route<dynamic> route) => false);
                            // }
                          },
                          child: Text(
                            'Reset Password',
                            style: TextStyle(
                              color: fromHex(maroon),
                              fontWeight: FontWeight.w800,
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        model.state == ViewState.Idle
                            ? RaisedButton(
                                elevation: 5.0,
                                onPressed: () async {
                                  if (_formKey.currentState!.validate()) {
                                    Map<String, String> data = {
                                      "email": "${_userName.text}",
                                      "password": "${_password.text}"
                                    };
                                    try {
                                      var r = await model.loginUser(data);
                                      if (r.success!) {
                                        showToast("success");
                                        setCurrentUser(r.createAccountData!);
                                        setToken(r.token ?? "");
                                        Navigator.pushNamedAndRemoveUntil(
                                            context,
                                            HomePage.tag,
                                            (Route<dynamic> route) => false);
                                      }
                                    } catch (e) {
                                      try {
                                        Map<String, dynamic> error =
                                            jsonDecode(e.toString());
                                        showToast(
                                            error['errors'][0]['message']);
                                      } catch (a) {}
                                    }
                                  }
                                },
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0)),
                                color: fromHex(maroon),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 10.0),
                                  child: Center(
                                    child: Row(
                                      children: [
                                        Text(
                                          'Login',
                                          style: TextStyle(
                                              color: fromHex(white),
                                              fontSize: 18),
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Icon(
                                          Icons.arrow_forward_ios,
                                          size: 16,
                                          color: fromHex(white),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              )
                            : loader(),
                      ],
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Don't have an account? "),
                        GestureDetector(
                          onTap: () {
                            if (model.state == ViewState.Idle) {
                              Navigator.pushNamedAndRemoveUntil(
                                  context,
                                  RegistrationPage.tag,
                                  (Route<dynamic> route) => false);
                            }
                          },
                          child: Text(
                            'Create Account',
                            style: TextStyle(
                              color: fromHex(maroon),
                              fontWeight: FontWeight.w800,
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
