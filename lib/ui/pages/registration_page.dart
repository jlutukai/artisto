import 'dart:convert';

import 'package:artisto/core/viewmodels/login_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../utils/constants.dart';
import 'base_view.dart';
import 'home_page.dart';
import 'login_page.dart';

class RegistrationPage extends StatefulWidget {
  static const tag = 'registration';
  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  TextEditingController _fname = TextEditingController();
  TextEditingController _oname = TextEditingController();
  TextEditingController _email = TextEditingController();
  TextEditingController _phone = TextEditingController();
  TextEditingController _password = TextEditingController();
  TextEditingController _confirmPassword = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  String pass = "";
  String confirmPass = "";

  @override
  void initState() {
    // deleteAddUserData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<LoginModel>(
      builder: (context, model, child) => Stack(
        children: [
          Scaffold(
            backgroundColor: fromHex(background),
            resizeToAvoidBottomInset: true,
            body: Column(
              children: [
                Stack(
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.height * 0.25,
                      decoration: BoxDecoration(
                        color: fromHex(maroon),
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(55),
                        ),
                      ),
                    ),
                    Positioned(
                      right: -MediaQuery.of(context).size.width * 0.10,
                      bottom: 0,
                      child: SvgPicture.asset(
                        "assets/logo.svg",
                        color: fromHex(white).withOpacity(0.15),
                        width: MediaQuery.of(context).size.height * 0.20,
                        height: MediaQuery.of(context).size.height * 0.20,
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        height: MediaQuery.of(context).size.height * 0.25,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 30, bottom: 30),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Row(
                                children: [
                                  Text(
                                    "Join us today",
                                    style: TextStyle(
                                        color: fromHex(white),
                                        fontWeight: FontWeight.w900,
                                        fontSize: 24),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  Text(
                                    "Register to continue...",
                                    style: TextStyle(
                                        color: fromHex(white),
                                        fontWeight: FontWeight.w100,
                                        fontSize: 12),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Expanded(child: _formDetails(context, model)),
              ],
            ),
          ),
          Positioned(
            left: 20,
            top: 55,
            child: FloatingActionButton(
              foregroundColor: Colors.white,
              backgroundColor: fromHex(dark_purple),
              onPressed: () {
                Navigator.pushNamedAndRemoveUntil(
                    context, LoginPage.tag, (Route<dynamic> route) => false);
              },
              mini: true,
              tooltip: "go back",
              child: Icon(
                Icons.keyboard_backspace_sharp,
                color: fromHex(white),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _formDetails(BuildContext context, LoginModel model) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            SizedBox(
              height: 35,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                color: fromHex(light_maroon).withOpacity(0.05),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(15),
                ),
              ),
              child: TextFormField(
                controller: _fname,
                keyboardType: TextInputType.text,
                textCapitalization: TextCapitalization.words,
                style: TextStyle(color: Colors.black),
                validator: (value) {
                  if (value!.isEmpty) {
                    return "Please Enter First Name";
                  }
                  return null;
                },
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white10,
                  isDense: true,
                  hintText: "e.g. John",
                  labelText: "First Name",
                  labelStyle: TextStyle(color: fromHex(black)),
                  hintStyle: TextStyle(
                    color: fromHex(grey),
                  ),
                  border: getBorder(),
                  focusedBorder: getEnabledBorder(),
                  enabledBorder: getBorder(),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                color: fromHex(light_maroon).withOpacity(0.05),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(15),
                ),
              ),
              child: TextFormField(
                controller: _oname,
                keyboardType: TextInputType.text,
                textCapitalization: TextCapitalization.words,
                style: TextStyle(color: Colors.black),
                validator: (value) {
                  if (value!.isEmpty) {
                    return "Please Enter Last Name";
                  }
                  return null;
                },
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white10,
                  isDense: true,
                  hintText: "e.g. Doe",
                  labelText: "Last Name",
                  labelStyle: TextStyle(color: fromHex(black)),
                  hintStyle: TextStyle(
                    color: fromHex(grey),
                  ),
                  border: getBorder(),
                  focusedBorder: getEnabledBorder(),
                  enabledBorder: getBorder(),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                color: fromHex(light_maroon).withOpacity(0.05),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(15),
                ),
              ),
              child: TextFormField(
                controller: _phone,
                keyboardType: TextInputType.phone,
                style: TextStyle(color: Colors.black),
                validator: (value) {
                  if (value!.isEmpty) {
                    return "Please Enter phone number";
                  }
                  return null;
                },
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white10,
                  isDense: true,
                  hintText: "e.g. 07 XXX XXX",
                  labelText: "Phone Number",
                  labelStyle: TextStyle(color: fromHex(black)),
                  hintStyle: TextStyle(
                    color: fromHex(grey),
                  ),
                  border: getBorder(),
                  focusedBorder: getEnabledBorder(),
                  enabledBorder: getBorder(),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                color: fromHex(light_maroon).withOpacity(0.05),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(15),
                ),
              ),
              child: TextFormField(
                controller: _email,
                keyboardType: TextInputType.emailAddress,
                style: TextStyle(color: Colors.black),
                validator: (value) {
                  if (value!.isEmpty) {
                    return "Please Enter Email Address";
                  }
                  return null;
                },
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white10,
                  isDense: true,
                  hintText: "e.g. johndoe@exaple.com",
                  labelText: "Email Address",
                  labelStyle: TextStyle(color: fromHex(black)),
                  hintStyle: TextStyle(
                    color: fromHex(grey),
                  ),
                  border: getBorder(),
                  focusedBorder: getEnabledBorder(),
                  enabledBorder: getBorder(),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                color: fromHex(light_maroon).withOpacity(0.05),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(15),
                ),
              ),
              child: TextFormField(
                controller: _password,
                keyboardType: TextInputType.visiblePassword,
                style: TextStyle(color: Colors.black),
                obscureText: true,
                validator: (value) {
                  if (value!.isEmpty) {
                    return "Please Enter your password";
                  }
                  if (value != confirmPass) {
                    return "Passwords do not match";
                  }
                  return null;
                },
                onChanged: (value) {
                  setState(() {
                    pass = value;
                  });
                },
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white10,
                  isDense: true,
                  hintText: "e.g. Enter your password",
                  labelText: "Password",
                  labelStyle: TextStyle(color: fromHex(black)),
                  hintStyle: TextStyle(
                    color: fromHex(grey),
                  ),
                  border: getBorder(),
                  focusedBorder: getEnabledBorder(),
                  enabledBorder: getBorder(),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                color: fromHex(light_maroon).withOpacity(0.05),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(15),
                ),
              ),
              child: TextFormField(
                controller: _confirmPassword,
                keyboardType: TextInputType.visiblePassword,
                obscureText: true,
                style: TextStyle(color: Colors.black),
                validator: (value) {
                  if (value!.isEmpty) {
                    return "Please Re-Enter Your Password";
                  }
                  if (value != pass) {
                    return "Passwords do not match";
                  }
                  return null;
                },
                onChanged: (value) {
                  setState(() {
                    confirmPass = value;
                  });
                },
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white10,
                  isDense: true,
                  hintText: "e.g. Re-enter your password",
                  labelText: "Confrim Password",
                  labelStyle: TextStyle(color: fromHex(black)),
                  hintStyle: TextStyle(
                    color: fromHex(grey),
                  ),
                  border: getBorder(),
                  focusedBorder: getEnabledBorder(),
                  enabledBorder: getBorder(),
                ),
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      // model.state == ViewState.Idle
                      //     ?
                      RaisedButton(
                        elevation: 5.0,
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            Map<String, String> data = {
                              "firstName": "${_fname.text}",
                              "lastName": "${_oname.text}",
                              "phoneNumber": "${_phone.text}",
                              "email": "${_email.text}",
                              "password": "$pass"
                            };
                            try {
                              var r = await model.registerUser(data);
                              if (r.success!) {
                                showToast("Success");
                                setCurrentUser(r.createAccountData!);
                                setToken(r.token ?? "");
                                Navigator.pushNamedAndRemoveUntil(
                                    context,
                                    HomePage.tag,
                                    (Route<dynamic> route) => false);
                              }
                            } catch (e) {
                              try {
                                Map<String, dynamic> error =
                                    jsonDecode(e.toString());
                                showToast(error['errors'][0]['message']);
                              } catch (a) {}
                            }
                          }
                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0)),
                        color: fromHex(maroon),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10.0),
                          child: Center(
                            child: Row(
                              children: [
                                Text(
                                  'Register',
                                  style: TextStyle(
                                      color: fromHex(white), fontSize: 18),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Icon(
                                  Icons.arrow_forward_ios,
                                  size: 16,
                                  color: fromHex(white),
                                ),
                              ],
                            ),
                          ),
                        ),
                      )
                      // : loader(),
                    ],
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Already have an account? "),
                      GestureDetector(
                        onTap: () {
                          // if (model.state == ViewState.Idle) {
                          Navigator.pushNamedAndRemoveUntil(context,
                              LoginPage.tag, (Route<dynamic> route) => false);
                          // }
                        },
                        child: Text(
                          'Log In',
                          style: TextStyle(
                            color: fromHex(dark_purple),
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
            SizedBox(
              height: 35,
            ),
          ],
        ),
      ),
    );
  }
}
