import 'package:artisto/ui/widgets/home/events_page.dart';
import 'package:artisto/ui/widgets/home/feeds_page.dart';
import 'package:artisto/ui/widgets/home/live_page.dart';
import 'package:artisto/ui/widgets/home/profile_page.dart';
import 'package:artisto/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

final Color backgroundColor = Color(0xFF4A4A58);

class HomePage extends StatefulWidget {
  static const tag = 'home';
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;
  late PageController _pageController;
  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  void onDataChange(int index) {
    print("pressed");
    setState(() {
      _pageController.animateToPage(index,
          duration: Duration(milliseconds: 300), curve: Curves.easeInToLinear);
    });
  }

  onWillPop(context) async {
    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: fromHex(background),
      body: PageView(
        controller: _pageController,
        onPageChanged: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
        children: <Widget>[
          FeedsPage(),
          LivePage(),
          EventsPage(),
          ProfilePage(),
        ],
      ),
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: fromHex(background),
          primaryColor: fromHex(maroon),
          textTheme: Theme.of(context).textTheme.copyWith(
                caption: new TextStyle(color: fromHex(maroon)),
              ),
        ),
        child: BottomNavigationBar(
          elevation: 0,
          currentIndex: _currentIndex,
          onTap: (int index) {
            setState(() {
              this._currentIndex = index;
            });
            _navigateToScreens(index);
          },
          type: BottomNavigationBarType.fixed,
          showUnselectedLabels: false,
          selectedFontSize: 12,
          selectedItemColor: fromHex(dark_purple),
          selectedLabelStyle:
              TextStyle(fontWeight: FontWeight.w800, color: fromHex(maroon)),
          items: [
            new BottomNavigationBarItem(
              backgroundColor: Colors.white,
              icon: _currentIndex == 0
                  ? Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: fromHex(maroon).withOpacity(0.3),
                      ),
//                            padding: EdgeInsets.all(5),
                      child: Icon(
                        Icons.home,
                        size: 24,
                        color: fromHex(dark_purple),
                      ),
                    )
                  : Container(
                      color: Colors.transparent,
                      child: Icon(
                        Icons.home_outlined,
                        size: 24,
                        color: fromHex(grey),
                      ),
                    ),
              label: "Home",
            ),
            BottomNavigationBarItem(
              icon: _currentIndex == 1
                  ? Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: fromHex(maroon).withOpacity(0.3),
                      ),
//                            padding: EdgeInsets.all(5),
                      child: Icon(
                        Icons.live_tv,
                        size: 24,
                        color: fromHex(dark_purple),
                      ),
                    )
                  : Container(
                      color: Colors.transparent,
                      child: Icon(
                        Icons.live_tv,
                        size: 24,
                        color: fromHex(grey),
                      ),
                    ),
              label: "Live",
            ),
            new BottomNavigationBarItem(
              icon: _currentIndex == 2
                  ? Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: fromHex(maroon).withOpacity(0.3),
                      ),
//                            padding: EdgeInsets.all(5),
                      child: Icon(
                        Icons.event_note,
                        size: 24,
                        color: fromHex(dark_purple),
                      ),
                    )
                  : Container(
                      color: Colors.transparent,
                      child: Icon(
                        Icons.event_note_outlined,
                        size: 24,
                        color: fromHex(grey),
                      ),
                    ),
              label: "Events",
            ),
            BottomNavigationBarItem(
              icon: _currentIndex == 3
                  ? Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: fromHex(maroon).withOpacity(0.3),
                      ),
//                            padding: EdgeInsets.all(5),
                      child: Icon(
                        Icons.person,
                        size: 24,
                        color: fromHex(dark_purple),
                      ),
                    )
                  : Container(
                      color: Colors.transparent,
                      child: Icon(
                        Icons.person,
                        size: 24,
                        color: fromHex(grey),
                      ),
                    ),
              label: "Profile",
            ),
          ],
        ),
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  void _navigateToScreens(int index) {
    setState(() {
      _currentIndex = index;
      _pageController.animateToPage(index,
          duration: Duration(milliseconds: 100), curve: Curves.ease);
    });
  }

  _widget(String s) {
    return Container(
      color: Colors.white,
      child: Column(
        children: [
          Expanded(
            child: Center(child: Text(s)),
          )
        ],
      ),
    );
  }
}
